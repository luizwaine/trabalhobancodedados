/**
 * @author Vallysys
 *
 */
package hyperion.conexao;

import java.sql.*;

public class Conexao {
	public Connection conexao(String user,String password) throws SQLException, ClassNotFoundException {
		Connection con = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection(
					"jdbc:oracle:thin:@localhost:1521/xe",user,password);
		} catch (Exception e) {
			throw e;
		}
		return con;
	}
}