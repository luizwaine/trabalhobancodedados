CREATE OR REPLACE TRIGGER valida_custo 
BEFORE INSERT OR UPDATE ON CONSULTA FOR EACH ROW

DECLARE
   v_normal NUMERIC;
   v_minimo NUMERIC;
BEGIN
   
   SELECT M.NORMAL INTO v_normal 
     FROM MEDICO M 
    WHERE M.CRM = :new.CRM
      AND M.UF  = :new.UF;

   SELECT M.MINIMO INTO v_minimo 
     FROM MEDICO M 
    WHERE M.CRM = :new.CRM
      AND M.UF  = :new.UF;

   IF :new.CUSTO >= v_minimo AND :new.CUSTO <= v_normal THEN
    RAISE_APPLICATION_ERROR(123456,'O valor do campo custo não é um valor permitido.');
   END IF;

END;
/


CREATE OR REPLACE PROCEDURE PRC_INSEIR_USUARIO(NOME IN VARCHAR, SENHA IN VARCHAR,TIPO IN NUMERIC) IS 

   V_ROLE VARCHAR;

   BEGIN

      IF TIPO = 1 THEN
          V_ROLE := 'MEDICO';
      ELSE
          V_ROLE := 'SECRETARIA';
      END IF; 
   
      EXECUTE IMMEDIATE('CREATE USER ' || NOME || 'IDENTIFIED BY ' || SENHA || ' DEFAULT TABLESPACE USERS');
      COMMIT;
      EXECUTE IMMEDIATE('GRANT ' || V_ROLE || ' TO ' || NOME);
      COMMIT;

END PRC_INSEIR_USUARIO;
/
